# Middlewares e Cabeçalhos HTTP

## Cabeçalhos HTTP (Headers)

Os cabeçalhos HTTP permitem que o cliente e o servidor passem informações adicionais com a solicitação ou a resposta HTTP. Um cabeçalho de solicitação é composto por seu nome case-insensitive (não diferencia letras maiúsculas e minúsculas), seguido por dois pontos ':' e pelo seu valor (sem quebras de linha).  Espaços em branco antes do valor serão ignorados.

Cabeçalhos podem ser classificados de acordo com os seus contextos:
- Cabeçalho genérico: Cabeçalhos que podem ser usados tanto em solicitações quanto em respostas, porém sem relação com os dados eventualmente transmitidos no corpo da mensagem.
- Cabeçalho de solicitação: Cabeçalhos contendo mais informação sobre o recurso a ser obtido ou sobre o próprio cliente.
- Cabeçalho de resposta: Cabeçalhos contendo informação adicional sobre a solicitação, como a sua localização ou sobre o servidor.
- Cabeçalho de entidade: Cabeçalhos contendo mais informação sobre o conteúdo da entidade, como o tamanho do conteúdo ou o seu MIME-type.

Alguns exemplos:

- **Cache-Control:** Especifica diretivas para mecanismos de cache em requisições e respostas.
- **Expires:** A data/hora depois que a resposta é considerada obsoleta.
- **Connection**: Controla se uma conexão de rede continua ou não aberta após o término da transação atual.
- **Keep-Alive**: Controla por quanto tempo uma conexão persistente deve permanecer aberta.
- **Accept**: Informa ao servidor sobre os tipos de dados que podem ser enviados de volta. Isto é MIME-type.
- **Accept-Charset**: Informa ao servidor sobre qual conjunto de caracter o cliente é capaz de entender.
- **Cookie:** Contém cookies HTTP armazenados préviamente enviados pelo servidor com o cabeçalho `Set-Cookie`.
- **Set-Cookie:** Envia cookies do servidor para o agente de usuário.
- **Access-Control-Allow-Origin**: Indica se a resposta pode ser compartilhada.
- **Content-Disposition:** Em uma resposta HTTP regular, é um cabeçalho que indica se o conteúdo deve ser exibido em linha no navegador, ou seja, como uma página da Web ou como parte de uma página da Web ou como um anexo, que é baixado e salvo localmente.
- **Content-Type:** Indica o tipo de mídia do recurso.
- **Content-Encoding:** Usado para especificar o algoritmo de compressão.


## Usando Middlewares com Express

O Express é uma estrutura web de roteamento e middlewares que tem uma funcionalidade mínima por si só: Um aplicativo do Express é essencialmente uma série de chamadas de funções de middleware.

Funções de Middleware são funções que tem acesso ao objeto de solicitação (`req`), o objeto de resposta (`res`), e a próxima função de middleware no ciclo solicitação-resposta do aplicativo. A próxima função middleware é comumente denotada por uma variável chamada `next`.

Funções de middleware podem executar as seguintes tarefas:
- Executar qualquer código.
- Fazer mudanças nos objetos de solicitação e resposta.
- Encerrar o ciclo de solicitação-resposta.
- Chamar a próxima função de middleware na pilha.

Se a atual função de middleware não terminar o ciclo de solicitação-resposta, ela precisa chamar `next()` para passar o controle para a próxima função de middleware. Caso contrário, a solicitação ficará suspensa.

Um aplicativo Express pode usar diferentes tipos de middlewares, porém iremos focar no `Middleware de nível do aplicativo`, vinculando middlewares de nível do aplicativo a uma instância do objeto `app` usando a função `app.use()`:

```
const app = express();

app.use((req, res, next) => {
  console.log('Time:', Date.now());
  next();
});
```

Podemos montar middlewares em uma rota:

```
app.use('/user/:id', (req, res, next) => {
  console.log('id param:', req.params.id);
  next();
});
```
Neste caso, `:id` é um parâmetro dinâmico que pode ser acessado via `req.params.id`.

## Docs:
- [Cabeçalhos HTTP](https://developer.mozilla.org/pt-BR/docs/Web/HTTP/Headers)
- [Entendendo um pouco mais sobre o protocolo HTTP](https://nandovieira.com.br/entendendo-um-pouco-mais-sobre-o-protocolo-http)
- [Usando middlewares](https://expressjs.com/pt-br/guide/using-middleware.html)
- [Escrevendo middlewares pra uso em aplicativos do Express](https://expressjs.com/pt-br/guide/writing-middleware.html)
- [Entendendo como funciona os middlewares do Express](https://udgwebdev.com/entendendo-como-funciona-os-middlewares-do-express/)
- [express async await](https://lombardo-chcg.github.io/languages/2017/09/18/express-async-await.html)


## Desafio

### Entrega:

Vocês deverão criar um endpoint que converte um JSON pra XML, com base no retorno da API **Via CEP**, utilizando a **Pesquisa de CEP**:

- `https://viacep.com.br/ws/{{estado}}/{{cidade}}/{{logradouro}}/json/`
- [Via CEP](https://viacep.com.br/)

Para isso, vocês criarão um middleware que faz o `GET` na API **Via CEP**, um middleware que converte para XML e a rota que retorna o XML.

A rota deve conter um dos seguintes caminhos: 
- `localhost:3000/{{estado}}/{{cidade}}/{{logradouro}}`
- `localhost:3000?estado={{estado}}&cidade={{cidade}}&logradouro={{logradouro}}`

O XML deverá ser um item baixável ao ser acessado pelo navegador, para isso, vocês faram o uso dos `headers`, e deverão usar no mínimo 2 dos headers apresentados em sala, para a aplicação funcionar corretamente.

**Obs.:** Para o desafio, desconsiderem os headers `Cookie` e `Set-Cookie`.

Os headers deverão ter os valores corretos, de acordo a resposta do endpoint.

Para converter para XML, vocês podem usar bibliotecas do npm.

É obrigatório a criação de uma pasta desafio contendo os scripts.

### Para se destacar:

Criar rotas do tipo `get` com parâmetros que possam retornar os seguintes tipos, cada um com seus respectivos headers:

- um `xml` baixável
- um `csv` baixável
- um `json`

Criar tratativas de erros e exceções.